package frc.robot.commands.lift;

import java.util.HashSet;
import java.util.Set;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class Extend implements Command 
{

    private Set<Subsystem> requirements;

    public Extend(){
        requirements = new HashSet<>();
    }

    public Set<Subsystem> getRequirements(){
        return requirements;
    }

    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    //Raises the pistons
    public void initialize(){
        Robot.lift.raisePistons();
    }

    public void execute(){
        /*
        if (Robot.shooter.getEncoderValue() > -0.03) {
            Robot.shooter.moveTurret(-0.6);
        }
        else {
            Robot.shooter.moveTurret(0);
        }
        */

    }

    public boolean isFinished(){
        return true;
    }
    public void end(boolean f){
        if(f){
            Robot.lift.liftWithMotor(0);
            //interrupt
        }
        else{
            Robot.lift.liftWithMotor(0);
            //end
        }
    }
}
