package frc.robot.commands.lift;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class Retract implements Command 
{

    private Set<Subsystem> requirements;

    public Retract(Subsystem ... s) 
    {
        requirements = new HashSet<>();
        for (Subsystem sub: s) 
        {
            requirements.add(sub);
        }
    }

    public Set<Subsystem> getRequirements() 
    {
        return requirements;
    }

    public boolean hasRequirement(Subsystem s) 
    {
        return requirements.contains(s);
    }

    //Starts lowering the pistons
    public void initialize()
    {
    }

    //Drives motor to lift the robot - will utilize a joystick (maybe) in the future
    public void execute() 
    {
        Robot.lift.liftWithMotor(-.6);
    }

    public void end(boolean f) 
    {

        if(f) 
        {
            Robot.lift.liftWithMotor(0);
            //Robot.lift.stopPistons();
        }
        else 
        {
            Robot.lift.liftWithMotor(0);
        }

    }
   
}
