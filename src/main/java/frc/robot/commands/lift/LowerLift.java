package frc.robot.commands.lift;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class LowerLift implements Command 
{
    private Set<Subsystem> requirements;

    public LowerLift(){
        requirements = new HashSet<>();
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void initialize(){
        Robot.lift.freeFloatPistons();
    }
    public void execute(){
    }
    public boolean isFinished(){
        return true;
    }

    public void end(boolean f) 
    {
        if(f){
            //SmartDashboard.putBoolean("Extend_interrupt",true);
        }
        else{
            //SmartDashboard.putBoolean("Extend_end",true);
        }
    }
}
