package frc.robot.commands.pixy;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;


public class TriggerAlignPixy implements Command {

    public Set<Subsystem> requirements;
    PixyAlignCommand pixyAlign;
    public TriggerAlignPixy(Subsystem ... s) {
        requirements = new  HashSet<>();
            for (Subsystem sub: s) {
                requirements.add(sub);
            }
            pixyAlign  = new PixyAlignCommand(Robot.drivetrain);
            pixyAlign.cancel();
        }
 
    public void initialize(){ 
    }
 
    public Set<Subsystem> getRequirements(){
        return requirements;
    }

    public void execute() {
        if(Robot.oi.getDriverLeftTrigger() && !(pixyAlign.isScheduled())){
            pixyAlign.schedule();
         } else if(!(Robot.oi.getDriverLeftTrigger()) && pixyAlign.isScheduled()){
            pixyAlign.cancel();
         }
    }             
        
            
    

    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void end(boolean f){
        if(f){
            //SmartDashboard.putBoolean("Align_interrupt",true);
        }else{
            //SmartDashboard.putBoolean("Align_end",true);
        }
    }
   

}
