package frc.robot.commands.pixy;

import java.util.HashSet;
import java.util.Set;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
import frc.robot.sensors.pixy.*;


public class PixyAlignCommand implements Command {

    public Set<Subsystem> requirements;

    public PixyAlignCommand(Subsystem ... s) {
        requirements = new  HashSet<>();
            for (Subsystem sub: s) {
                requirements.add(sub);
            }
        }
 
    public void initialize(){ 
    }
 
    public Set<Subsystem> getRequirements(){
        return requirements;
    }

    public void execute() {
        SmartDashboard.putBoolean("Align_exec",true);
        //trigger press
        if (Vision.blobs.length > 0 && Vision.blobs[0].getX() != 160) {

            if (Vision.blobs[0].getX() < 160) {
                    SmartDashboard.putNumber("align num",(.15+(.6*Math.abs((Vision.blobs[0].getX()-160.0) / 160.0))));
                    Robot.drivetrain.drive(.15+(.6* Math.abs((Vision.blobs[0].getX()-160.0) / 160.0)), Robot.oi.getDriverLeftJoystickY());

            }
            else if (Vision.blobs[0].getX() > 160) {
                    SmartDashboard.putNumber("align num",(-.15-(.6*Math.abs((Vision.blobs[0].getX()-160.0) / 160.0))));
                    Robot.drivetrain.drive(-.15-(.6 * Math.abs((Vision.blobs[0].getX()-160.0) / 160.0)), Robot.oi.getDriverLeftJoystickY());
    
            }
        
    }
    }             
        
            
    

    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void end(boolean f){
        if(f){
            SmartDashboard.putBoolean("Align_interrupt",true);
        }else{
            SmartDashboard.putBoolean("Align_end",true);
        }
    }
   

}
