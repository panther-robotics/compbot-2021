package frc.robot.commands.shooter;
import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj.GenericHID.Hand;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
//manual align for the turret if the limelight fails
public class ManualAlign implements Command{

    private Set<Subsystem> requirements;
    public ManualAlign(Subsystem ... s){
        requirements = new HashSet<>();
        for (Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void initialize(){
    }
    //Manually spins the turret & sets hood
    public void execute(){
       // Robot.shooter.moveTurret(Robot.oi.getShooterLeftJoystickX());
       // Robot.shooter.setHoodManual(Robot.oi.xboxShoot.getY(Hand.kRight));
    }
    public boolean isFinished(){
     return false;
    }
    public void end(boolean f){
        if(f){
            //interrupt
        }
        else{
            //end
        }
    }
}
