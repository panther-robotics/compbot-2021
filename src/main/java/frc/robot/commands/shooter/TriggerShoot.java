package frc.robot.commands.shooter;

import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;


public class TriggerShoot implements Command {
    Set<Subsystem> requirements;
    AlignShootAll alignObject; 
    ShootAll shootAll;
    public TriggerShoot(){
        requirements = new HashSet<>();
        alignObject = new AlignShootAll();
        shootAll = new ShootAll();
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void init(){
    }
    //Monitoring for shooter trigger press
    public void execute(){
        SmartDashboard.putBoolean("align object", shootAll.isScheduled());
        if(Robot.oi.getShooterRightTrigger() && !(shootAll.isScheduled())){
           shootAll.schedule();
        } else if(!(Robot.oi.getShooterRightTrigger()) && shootAll.isScheduled()){
           shootAll.cancel();
        }
    }
    public boolean isFinished(){
        return false;
    }
}