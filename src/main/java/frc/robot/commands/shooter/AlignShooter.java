package frc.robot.commands.shooter;

import java.util.HashSet;
import java.util.Set;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class AlignShooter implements Command {

    private Set<Subsystem> requirements;

    private boolean leftRight = true;

    public AlignShooter(Subsystem ... s){
        requirements = new HashSet<>();
        for (Subsystem sub: s){
            requirements.add(sub);
        }
    }

    public Set<Subsystem> getRequirements(){
        return requirements;
    }

    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void initialize(){
        leftRight = true;
    }

    public void execute(){
        /*SmartDashboard.putBoolean("Align_exec",true);
        SmartDashboard.putBoolean("Left/Right",leftRight);
       
        *Runs if the tx is not within 1 degree, which it will either
        *run at tx*.04 or .4, dependent on tx.
        */


        //if no target is detected
        if (Robot.limelight.targetDetected() == 0.0) {
        /*
            if (Robot.shooter.getEncoderValue() > -2.1 && leftRight) {
                Robot.shooter.moveTurretWithLimelight(-0.65);
            
                if (Robot.shooter.getEncoderValue() <= -1.9 && leftRight) {
                    leftRight = false;
                }
            }
            else if (Robot.shooter.getEncoderValue() < 1.9 && !leftRight) {
                Robot.shooter.moveTurretWithLimelight(0.65);

                if (Robot.shooter.getEncoderValue() >= 1.7 && !leftRight) {
                    leftRight = true;
                }
            }
            */
        }
        /*
        //if a target is detected and the angle is above 1
        else if (Math.abs(Robot.limelight.getAngleX()) > 1.0) {
            if (Math.abs(Robot.limelight.getAngleX()) * 0.04 > 0.4){
                //Robot.drivetrain.drive(Robot.limelight.getAngleX() * 0.04, 0);
                Robot.shooter.moveTurretWithLimelight(Robot.limelight.getAngleX() * 0.04);
            }
            else{
                //Robot.drivetrain.drive(Math.signum(Robot.limelight.getAngleX()) * 0.4, 0);
                Robot.shooter.moveTurretWithLimelight(Math.signum(Robot.limelight.getAngleX()) * 0.3);
            }
        }
        */
    }

    public boolean isFinished(){
      return (Robot.limelight.isAligned());
    }

    public void end(boolean f){   
        if(f){
            SmartDashboard.putBoolean("Align_interrupt",true);
        }
        else{
            //Robot.shooter.moveTurret(0);
            SmartDashboard.putBoolean("Align_end",true);
        }
    }
}
