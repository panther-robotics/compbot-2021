package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import frc.robot.Robot;
//manual align for the turret if the limelight fails
public class FullAlign extends ParallelCommandGroup {
    AlignShooter align;
    SetHood setHood;
    public FullAlign() 
    {
        align = new AlignShooter(Robot.shooter);
        setHood = new SetHood();
        addCommands(align, setHood);


    }

    public void initialize()
    {
    }

    public void execute() 
    {
    
    }

    public void end(boolean f)
    {
        if(f){
            SmartDashboard.putBoolean("ManualAlign_interrupt",true);
        }
        else{
            SmartDashboard.putBoolean("ManualAlign_end",true);
        }
    }
   
}
