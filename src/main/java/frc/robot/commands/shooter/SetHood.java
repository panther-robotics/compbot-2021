package frc.robot.commands.shooter;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
// import frc.robot.Robot;

public class SetHood implements Command {
    private Set<Subsystem> requirements;
    public SetHood(){
        requirements = new HashSet<>();
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    public void initialize(){
    }
    public void execute(){
       //Robot.shooter.setHood();

    }
    public boolean isFinished(){
        //check if set
        return false;
    }

    public void end(boolean f){
        if(f){
            //SmartDashboard.putBoolean("Align_interrupt",true);
        }
        else{

            //SmartDashboard.putBoolean("Align_end",true);
        }
    }
}
