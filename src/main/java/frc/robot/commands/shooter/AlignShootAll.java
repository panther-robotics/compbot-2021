package frc.robot.commands.shooter;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
// import frc.robot.Robot;

public class AlignShootAll extends SequentialCommandGroup {
    FullAlign fullAlign = new FullAlign();
    ShootWhileAligned shootWhileAligned = new ShootWhileAligned();
    
    public AlignShootAll(){
        addCommands(fullAlign, shootWhileAligned);
    }
    
    public void end(boolean f){
        if(f){
            //Interrupted
            //this is where conditions for shoot getting cut off
        } else{
            //end
        }
    }

}
