package frc.robot.commands.shooter;

// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import frc.robot.Robot;
import frc.robot.commands.collector.RotateStorage;
// import frc.robot.commands.shooter.FullAlign;
//manual align for the turret if the limelight fails

public class ShootWhileAligned extends ParallelCommandGroup {

    ShootAll shootAll;
    FullAlign fullAlign;
    public static RotateStorage rotateStorageNeg;
        
    public ShootWhileAligned(){
        rotateStorageNeg = new RotateStorage(true);   
        shootAll = new ShootAll();
        fullAlign = new FullAlign();
        addCommands(shootAll, fullAlign);
    }

    public void initialize(){
    }

    public void execute(){
    }

    public void end(boolean f){
        if(f){
            rotateStorageNeg.schedule();
            Robot.collector.retractSolenoid();
            Robot.collector.stopMovingCollector();
            Robot.shooter.stopRamp();
        }else{
            rotateStorageNeg.schedule();
            Robot.collector.stopMovingCollector();
            Robot.shooter.stopRamp();
            Robot.collector.retractSolenoid();
        }
    }
}
