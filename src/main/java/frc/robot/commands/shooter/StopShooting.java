package frc.robot.commands.shooter;
import java.util.HashSet;
import java.util.Set;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class StopShooting implements Command {
    Set<Subsystem> requirements;
    public StopShooting(Subsystem ... s){
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    public void init(){
        Robot.shooter.stopRamp();
    }
    public void execute(){
    }
    public boolean isFinished(){
        return true;
    }
}