package frc.robot.commands.shooter;

import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
import frc.robot.commands.collector.RotateStorage;


public class ShootAll implements Command {
    Set<Subsystem> requirements;
    public static RotateStorage rotateStorageNeg;
    public ShootAll(){
        requirements = new HashSet<>();
        }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    public void initialize(){
        SmartDashboard.putBoolean("Shoot one command", true);
        Robot.collector.collectorMotorTop.set(-1);
        Robot.collector.extendSolenoid();
    }
    public void execute(){
            Robot.shooter.shootAll();
            //Robot.collector.extendSolenoid();
    }
    public boolean isFinished(){
        //return (Robot.collector.checkStorage() < 1) && Robot.collector.ballCollected.get();
        return false;
    }
    public void end(boolean f){
        if(f){
            //rotateStorageNeg.schedule();
            //Robot.collector.retractSolenoid();
            Robot.collector.stopMovingCollector();
            Robot.shooter.stopRamp();
        }else{
            //rotateStorageNeg.schedule();
            /* chase comment for hopper
            Robot.collector.stopMovingCollector();
            Robot.shooter.stopRamp();
            Robot.collector.retractSolenoid();
            */
        }
        Robot.collector.retractSolenoid();
    }
}