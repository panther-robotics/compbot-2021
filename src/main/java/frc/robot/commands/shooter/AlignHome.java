package frc.robot.commands.shooter;

import java.util.HashSet;
import java.util.Set;

// import com.ctre.phoenix.motorcontrol.ControlMode;

// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class AlignHome implements Command {

    private Set<Subsystem> requirements;
    private boolean triedLeft=false;
    private boolean triedRight=false;
    public boolean set;
    public AlignHome(Subsystem ... s) 
    {
        requirements = new HashSet<>();
        for (Subsystem sub: s){
            requirements.add(sub);
        }
        set = false;
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    public void initialize(){
        //Sets the alignment to false every time the command starts
    }
    
    public void execute(){
        //Moves turret ring to origin
       /*
        if(!triedRight){
            if(Robot.shooter.getEncoderValue()>1.5){
                triedRight=true;
            }
            Robot.shooter.moveTurret(-.5);
        }else if(!triedLeft && Robot.shooter.getEncoderValue()>-1.5){
            Robot.shooter.moveTurret(.5);
        }else{
            Robot.shooter.moveTurret(0);
        }
        if (!Robot.shooter.hallEffect.get()) {
            Robot.shooter.setOrigin();
            Robot.shooter.moveTurret(0);
       }
        //Robot.shooter.setHoodZeroPos();
        */
        
    }
    public boolean isFinished(){
        //Asks if the shooter is currently aligned and hood is zeroed
        return !Robot.shooter.hallEffect.get(); //&& Robot.shooter.hoodLimit.get();
        //return true;
       
    }
    public void end(boolean f){
        /*
        if(f){
           //interrupt
           Robot.shooter.moveTurret(0);
           Robot.shooter.setHoodManual(0);
        }
        else{
            //End
            if (Robot.shooter.getEncoderValue() < .15) {
                Robot.shooter.moveTurret(.4);
            }
            else {
                Robot.shooter.moveTurret(0);
            }
            Robot.shooter.setHoodManual(0);
        }
    */}
}
