package frc.robot.commands.drivetrain;

// import frc.robot.Robot;
import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;

public class RotateToAbsolute implements Command {

    private Set<Subsystem> requirements;
    public RotateToAbsolute(Subsystem ... s){
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    public void initialize(){
    }
    public void execute() {
        //Robot.drivetrain.driveStraight(10000, 0);
    }
    public boolean isFinished(){
        return false;
    }
    public void end(boolean f){
        if(f){
            
        }else{
           
        }
    }
}
