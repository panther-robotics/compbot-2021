package frc.robot.commands.drivetrain;

import frc.robot.Robot;
import java.util.HashSet;
import java.util.Set;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;

public class DriveToRelative implements Command {
    private double inches;
    private boolean done;
    private Set<Subsystem> requirements;
    public DriveToRelative(double x, Subsystem ... s){
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
        done = false;
        inches = x;
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    public void initialize(){
    }
    public void execute() {

        done = Robot.drivetrain.moveInches(inches);

    }
    public boolean isFinished(){
        return done;
    }
    public void end(boolean f){
        if(f){
            SmartDashboard.putBoolean("interrupt",true);
        }else{
            SmartDashboard.putBoolean("end",true);
        }
    }
}
