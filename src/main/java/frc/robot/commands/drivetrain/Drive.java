package frc.robot.commands.drivetrain;

import frc.robot.Robot;
import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;

public class Drive implements Command {

    private Set<Subsystem> requirements;
    public Drive(Subsystem ... s){
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    public void initialize(){
    }
    public void execute() {
        // Robot.drivetrain.drive(Robot.oi.getDriverRightJoystickX(), Robot.oi.getDriverLeftJoystickY());
        Robot.drivetrain.drive(Robot.oi.getDriverLeftJoystickY(), Robot.oi.getDriverLeftJoystickX());
        SmartDashboard.putNumber("controller left y", Robot.oi.getDriverLeftJoystickY());
    }
    public boolean isFinished(){
        return false;
    }
    public void end(boolean f){
        if(f){
            //interrupt
        }else{
           //end
        }
    }
}
