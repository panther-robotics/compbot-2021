package frc.robot.commands.collector;

import java.util.HashSet;
import java.util.Set;

import com.ctre.phoenix.motorcontrol.ControlMode;

// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class RotateStorage implements Command {

    private Set<Subsystem> requirements;
    private boolean target;
    // private int halDeadband;
    public int storageMortorPosition;
    public RotateStorage(boolean positive){
        requirements = new HashSet<>();
        target = positive;
        // halDeadband = 114;
    }
    public Set<Subsystem> getRequirements() {
        return requirements;
    }
    public boolean hasRequirement(Subsystem s) {
        return requirements.contains(s);
    }
    public boolean reading;
    
    public void initialize(){
        if(!Robot.collector.hallEffectStorage.get()){
            reading = false;
        }
    }   
    
    public void execute() {
        //Robot.collector.rotateNinety(target);
        if(target){
            Robot.collector.storageMotor.set(ControlMode.PercentOutput, 100);
        }else{
            Robot.collector.storageMotor.set(ControlMode.PercentOutput, -100);
        }
        if(Robot.collector.storageMotor.getSelectedSensorVelocity() > 100){
            reading = true;
        }
        if(Robot.collector.storageMotor.getSelectedSensorVelocity() < -100){
            reading = true;
        }
        
    }
    public boolean isFinished(){
        return !Robot.collector.hallEffectStorage.get() && reading;
    }
    public void end(boolean f) {
        if(f) {
            
            Robot.collector.storageMotor.set(ControlMode.PercentOutput, 0);
            //SmartDashboard.putBoolean("StoreBalls_interrupt",true);
        }
        else {
            Robot.collector.storageMotor.set(ControlMode.PercentOutput, 0);
            //SmartDashboard.putBoolean("Align_end",true);
        }
    }
  
}
