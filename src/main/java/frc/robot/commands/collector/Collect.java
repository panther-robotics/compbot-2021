package frc.robot.commands.collector;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
// import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class Collect implements Command {

    private Set<Subsystem> requirements;
    public Collect(Subsystem ... s) {
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements() {
        return requirements;
    }
    public boolean hasRequirement(Subsystem s) {
        return requirements.contains(s);
    }
    public void initialize(){
    }

    public void execute() {
        //monitors the trigger value and schedules the collect commands
        //SmartDashboard.putBoolean("group scheduled", store.isScheduled());
        if(Robot.oi.getDriverRightTrigger()){ 
            Robot.collector.startCollecting();
            Robot.collector.extendSolenoid();
         } 
         else {
            Robot.collector.stopMovingCollector();
            Robot.collector.retractSolenoid();
         }
    }

    public boolean isFinished(){
       return false;
    }
}
