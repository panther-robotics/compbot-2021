package frc.robot.commands.collector;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class SpinCollectorMotor implements Command {

    private Set<Subsystem> requirements;

    public SpinCollectorMotor() {
        requirements = new HashSet<>();
    }
    public Set<Subsystem> getRequirements() {
        return requirements;
    }
    public boolean hasRequirement(Subsystem s) {
        return requirements.contains(s);
    }
    public void initialize(){
    }
    //Runs the collector at a set value
    public void execute() {
        Robot.collector.startCollecting();
    }
    public boolean isFinished(){
        return false;
    }
    public void end(boolean f) {
        if(f) {
            Robot.collector.stopMovingCollector();
            //SmartDashboard.putBoolean("Align_interrupt",true);
        }
        else {
            Robot.collector.stopMovingCollector();
            //SmartDashboard.putBoolean("Align_end",true); 
        }
    }
}
