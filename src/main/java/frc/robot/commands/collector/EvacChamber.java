package frc.robot.commands.collector;

import java.util.HashSet;
import java.util.Set;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class EvacChamber implements Command{

    private Set<Subsystem> requirements;
    public EvacChamber(Subsystem ... s){
        requirements = new HashSet<>();
        for (Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    //Shifts the Collector Pistons
    public boolean out;
    public void initialize(){
        out = false;
        Robot.collector.extendSolenoid();
    }

    public void execute(){
        Robot.collector.evacChamber();
        //Robot.collector.evacAll();
    }
    public boolean isFinished(){
        return false;
    }
    public void end(boolean f){
        if(f){
            Robot.collector.stopMovingCollector();
            Robot.collector.retractSolenoid();
            //SmartDashboard.putBoolean("Align_interrupt",true);
        }
        else{
            Robot.collector.stopMovingCollector();
            Robot.collector.retractSolenoid();
            //SmartDashboard.putBoolean("Align_end",true);
        }
    }
}
