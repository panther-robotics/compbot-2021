package frc.robot.commands.collector;

import java.util.HashSet;
import java.util.Set;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class EvacAll implements Command{
    public static RotateStorage rotateStorageNeg;
    private Set<Subsystem> requirements;
    public EvacAll(Subsystem ... s){
        requirements = new HashSet<>();
        for (Subsystem sub: s){
            requirements.add(sub);
        }
    }
    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    //Shifts the Collector Pistons
    public void initialize(){
    Robot.collector.extendSolenoid();
    add = true;
    counter = 0;
    Robot.collector.evacAll();
    rotateStorageNeg.schedule();
    }
    public boolean add;
    public int counter;
    public void execute(){
        Robot.collector.evacAll();
    }
    public boolean isFinished(){
        return false;
    }
    public void end(boolean f){
        if(f){
            Robot.collector.retractSolenoid();
            Robot.collector.stopMovingCollector();
        //interrupt        
    }
        else{
            Robot.collector.retractSolenoid();
            Robot.collector.stopMovingCollector();
        //end        
    }
    }
}
