package frc.robot;

import com.ctre.phoenix.motorcontrol.SensorCollection;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.commands.collector.EvacChamber;
import frc.robot.commands.collector.RotateStorage;
import frc.robot.commands.collector.Collect;
import frc.robot.commands.drivetrain.Drive;
import frc.robot.commands.drivetrain.DriveToRelative;
import frc.robot.commands.drivetrain.RotateToRelative;
// import frc.robot.commands.pixy.PixyAlignCommand;
// import frc.robot.commands.pixy.TriggerAlignPixy;
import frc.robot.commands.shooter.AlignHome;
import frc.robot.commands.shooter.AlignShootAll;
import frc.robot.commands.shooter.AlignShooter;
import frc.robot.commands.shooter.ManualAlign;
import frc.robot.commands.shooter.ShootAll;
// import frc.robot.commands.shooter.ShootAll;
// import frc.robot.commands.shooter.StopShooting;
import frc.robot.commands.shooter.TriggerShoot;
import frc.robot.sensors.Lidar;
import frc.robot.sensors.Limelight;
// import frc.robot.sensors.pixy.Vision;
import frc.robot.subsystems.Collector;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Lift;
import frc.robot.subsystems.Shooter;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
// import edu.wpi.first.wpilibj.DriverStation;

public class Robot extends TimedRobot{
  public static AlignShooter alignShooter;
  public static ShootAll shootAll; 
  public static AlignShootAll alignShootAll;
  public static DriveToRelative relativeDrive;
  public static RotateToRelative rotateRelative;
  public static DriveToRelative relativeDrive1;
  public static RotateToRelative rotateRelative1;
  public static DriveToRelative relativeDrive2;
  public static RotateToRelative rotateRelative2;
  public static DriveToRelative relativeDrive3;
  public static RotateToRelative rotateRelative3;
  public static DriveToRelative relativeDrive4;
  public static RotateToRelative rotateRelative4;
  public static SequentialCommandGroup seqGroup;
  public static CommandScheduler scheduler;
  public static Drivetrain drivetrain;
  public static Lift lift;
  public static Collector collector;
  public static Shooter shooter;
  public static OI oi;
  public static Limelight limelight;
  //public static Vision vision;
  public static TriggerShoot triggerShoot;
  public static Collect triggerCollect;
  public static AlignHome alignHome;
  public static Lidar lidar;
  //public static TriggerAlignPixy triggerPixyAlign;

    private void clearStickyFaults() {
    
    }
 
  @Override
  public void robotInit(){
    clearStickyFaults();
    elapsedTime = new Timer();
    elapsedTime.start();
    scheduler = CommandScheduler.getInstance();
    oi = new OI();
    collector = new Collector();
    lidar = new Lidar();
    drivetrain = new Drivetrain();
    lift = new Lift();
    shooter = new Shooter();
    limelight = new Limelight();
            alignShooter = new AlignShooter();
        shootAll = new ShootAll();

   //vision = new Vision();
   
    //Register Subsystems
    scheduler.registerSubsystem(drivetrain);
    scheduler.registerSubsystem(shooter);
    scheduler.registerSubsystem(collector);
    scheduler.registerSubsystem(lift);
    // scheduler.registerSubsystem(lidar);
    // scheduler.registerSubsystem(limelight);
    //scheduler.registerSubsystem(vision);
 
    triggerShoot = new TriggerShoot();
    triggerCollect = new Collect();
    // alignHome = new AlignHome(Robot.shooter);

    //create defalt commands
    scheduler.setDefaultCommand(collector, new Collect(collector));
    scheduler.setDefaultCommand(drivetrain, new Drive(drivetrain));
  
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
  public CommandScheduler instance(){
    return CommandScheduler.getInstance();
  }

@Override
public void teleopInit() {
  // alignHome.schedule();
  triggerCollect.schedule();
  //triggerPixyAlign.schedule();
  triggerShoot.schedule();
}

  @Override
  public void robotPeriodic(){
    scheduler.run();
    scheduler.enable();
    SmartDashboard.putBoolean("isEnabled", isEnabled());
  }
  
  @Override
  public void teleopPeriodic() {
    SmartDashboard.putNumber("periodic", SmartDashboard.getNumber("periodic",0)+1);
  }


  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    //What we want: Figure 8, target goal and shoot

    SmartDashboard.putBoolean("init", true);
    // alignHome.schedule();
    //first line
    // relativeDrive = new DriveToRelative(-1.1, Robot.drivetrain);
    // rotateRelative = new RotateToRelative(-.25, Robot.drivetrain);
    //turn into top part of 8
    /*
    rotateRelative1 = new RotateToRelative(-2.2, Robot.drivetrain);
    relativeDrive1 = new DriveToRelative(-1, Robot.drivetrain);
    //straight between 8
    rotateRelative2 = new RotateToRelative(-.25, Robot.drivetrain);
    relativeDrive2 = new DriveToRelative(-2, Robot.drivetrain);
    //turn into bottom part of 8
    rotateRelative3 = new RotateToRelative(3.4, Robot.drivetrain);
    relativeDrive3 = new DriveToRelative(-2.5, Robot.drivetrain);
    //drive to tower
    rotateRelative4 = new RotateToRelative(1, Robot.drivetrain);
    relativeDrive4 = new DriveToRelative(-5, Robot.drivetrain);
    */
    //call shooter
    // alignHome = new AlignHome(Robot.shooter);
    // alignShooter = new AlignShooter();
    // shootAll = new ShootAll();

    //AUTO CODE seqGroup = new SequentialCommandGroup(rotateRelative, relativeDrive, alignHome, alignShooter, shootAll);
    //relativeDrive1);
    //, relativeDrive1, rotateRelative2, relativeDrive2, rotateRelative3, relativeDrive3);
    //, rotateRelative4, relativeDrive4, alignHome, alignShootAll);
    // NEED THIS TO RUN THE CODE    seqGroup.schedule();
    // seqGroup1.schedule();
    // seqGroup2.schedule();
    // seqGroup3.schedule();

    
  }

  @Override
  public void autonomousPeriodic(){
    /*
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      default:
        // Put default auto code here
        break;
        */

        SmartDashboard.putNumber("Elapsed Time", elapsedTime.get());

      }

Command autonomousCommand;
public static Timer elapsedTime;

  @Override
  public void testPeriodic() {

  }
}
