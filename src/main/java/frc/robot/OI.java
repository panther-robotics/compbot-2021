package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
// import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
// import edu.wpi.first.wpilibj2.command.button.POVButton;
// import edu.wpi.first.wpilibj2.command.button.Trigger;

import frc.robot.commands.collector.EvacAll;
import frc.robot.commands.collector.EvacChamber;
import frc.robot.commands.collector.RotateStorage;
// import frc.robot.commands.collector.RotateStorage;
// import frc.robot.commands.drivetrain.DriveToRelative;
// import frc.robot.commands.lift.Extend;
import frc.robot.commands.lift.LowerLift;
import frc.robot.commands.lift.Retract;
import frc.robot.commands.lift.UnRetract;
import frc.robot.commands.shooter.AlignShooter;
// import frc.robot.commands.shooter.SetHood;
import frc.robot.commands.shooter.ShootAll;
// import frc.robot.commands.shooter.ShootJustOne;
import frc.robot.subsystems.Collector;

public class OI {
    
    private static final double DEADBAND = 0.05;
    
    /////////////////// Driver
    public XboxController xboxDrive = new XboxController(RobotMap.DRIVER_XBOX_PORT);
    public JoystickButton aButton = new JoystickButton(xboxDrive, 1);
    public JoystickButton bButton = new JoystickButton(xboxDrive, 2);
    public JoystickButton xButton = new JoystickButton(xboxDrive, 3);
    public JoystickButton yButton = new JoystickButton(xboxDrive, 4);

    public JoystickButton driverLBumper = new JoystickButton(xboxDrive, 5);
    public JoystickButton driverRBumper = new JoystickButton(xboxDrive, 6);
    public JoystickButton driverBackButton = new JoystickButton(xboxDrive, 7);
    public JoystickButton driverStartButton = new JoystickButton(xboxDrive, 8);

    /////////////////// Shooter
    public XboxController xboxShoot = new XboxController(RobotMap.SHOOTER_XBOX_PORT);
    public JoystickButton shooterRBumper = new JoystickButton(xboxShoot, 6);
    public JoystickButton shooterYButton = new JoystickButton(xboxShoot, 4);
    
    /////////////////// Driver triggers and joysticks
    public boolean getDriverLeftTrigger(){
        double driveLeftTrigger = xboxDrive.getTriggerAxis(Hand.kLeft);
        SmartDashboard.putNumber("drive left trigger", driveLeftTrigger);
        driveLeftTrigger = applyDeadband(driveLeftTrigger);
        return driveLeftTrigger>0;
    }
    public boolean getDriverRightTrigger(){
        double driveLeftTrigger = xboxDrive.getTriggerAxis(Hand.kRight);
        return applyDeadband(driveLeftTrigger)>0;
    }
    public double getDriverLeftJoystickY() {
        double LeftY = xboxDrive.getY(Hand.kLeft);
        return applyDeadband(LeftY);
    }
    public double getDriverLeftJoystickX() {
        double LeftX = xboxDrive.getX(Hand.kLeft);
        return applyDeadband(LeftX);
    }
    public double getDriverRightJoystickX() {
        double RightX = xboxDrive.getX(Hand.kRight);
        return applyDeadband(RightX);
    }

   /////////////////// Shooter triggers and joysticks
    public double getShooterLeftJoystickX(){
        double LeftX = xboxShoot.getX(Hand.kLeft);
        return applyDeadband(LeftX);
    }
    public double getShooterRightJoystickY(){
        double RightY = xboxShoot.getY(Hand.kRight);
        return applyDeadband(RightY);
    }
    public boolean getShooterLeftTrigger(){
        double shootLeftTrigger = xboxShoot.getTriggerAxis(Hand.kLeft);
        SmartDashboard.putNumber("shoot trigger left", shootLeftTrigger);
        return shootLeftTrigger>0;
    }
    public boolean getShooterRightTrigger(){
        double shootLeftTrigger = xboxShoot.getTriggerAxis(Hand.kRight);
        if(shootLeftTrigger != 0){
            return true;
        }else{
            return false;
        }
    }
    /////////////Global Deadband
    private double applyDeadband(double original) {
        if(Math.abs(original)<DEADBAND) {
            return 0;
        } else {
            return original;
        }
    }

    public OI(){
        //********************Driver******************//
        driverBackButton.whenHeld(new EvacChamber(Robot.collector));
        driverStartButton.whenPressed(new EvacAll(Robot.collector));
        //
        
        // aButton.whenHeld(new Retract(Robot.lift));
        // bButton.whenPressed(new LowerLift());
        // xButton.whenHeld(new UnRetract(Robot.lift));
        //********************Shooter*****************//
        //All shooter commands are joystick or trigger
        //shooterRBumper.whenHeld(new ShootJustOne());
        //shooterYButton.whenPressed(new ShootAll());
        driverRBumper.whenHeld(new ShootAll());
        //xButton.whenHeld(new SetHood());
        //xButton.whenPressed(new RotateStorage(180,Robot.shooter));
        //yButton.whenPressed(new RotateStorage(,Robot.shooter));
        /*aButton.whenPressed(new RotateStorage(true));
        bButton.whenPressed(new RotateStorage(false));
        driverLBumper.whenHeld(new SetHood());
        */
    }
}
