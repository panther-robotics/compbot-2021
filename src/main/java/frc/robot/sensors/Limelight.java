package frc.robot.sensors;

import edu.wpi.first.networktables.*;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;

public class Limelight implements Subsystem {
    NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
    NetworkTableEntry pipelineEntry = table.getEntry("pipeline");

    public Limelight() {
      Robot.scheduler.registerSubsystem(this);
    }
  
    @Override
    public void periodic() {
      setPipeline();

      //Used Limelight Variables
      SmartDashboard.putNumber("X Offset", Robot.limelight.getAngleX());
      SmartDashboard.putNumber("Tower found?", Robot.limelight.targetDetected());
      SmartDashboard.putNumber("distance", Robot.limelight.getDistance());
      SmartDashboard.putBoolean("Shooter Aligned?", Robot.limelight.isAligned());

      /*Unused Limelight Variables
      SmartDashboard.putNumber("ty", Robot.limelight.getAngleY());
      SmartDashboard.putNumber("ta", Robot.limelight.getArea());
      SmartDashboard.putNumber("ts", Robot.limelight.getSkew());
      SmartDashboard.putNumber("Pipeline", Robot.limelight.pipelineValue());
      */
    }

    //Gets the Limelight's x (left/right) offset
    public double getAngleX() {
      return table.getEntry("tx").getDouble(0.0);
    }
  
    //Gets the Limelight's y (up/down) offset
    public double getAngleY() {
      return table.getEntry("ty").getDouble(0.0);
    }
  
    //Gets the size of the tracked object 
    //(in percentage of screen covered)
    public double getArea() {
      return table.getEntry("ta").getDouble(0.0);
    }
  
    //Get the angle(?) of the object ***Currently unused, just for reference/future usage
    public double getSkew() {
      return table.getEntry("ts").getDouble(0.0);
    }

    //Asks if the target is detected
    public double targetDetected(){
      return table.getEntry("tv").getDouble(0.0);
    }

    //Returns true if the Limelight is aligned
    public boolean isAligned() {
      boolean alignCheck = false;
    
      if (Math.abs(Robot.limelight.getAngleX()) < 1 && targetDetected() == 1) {
        alignCheck = true;
      }
      else {
        alignCheck = false;
      }

      return alignCheck;
    }
    
    //Returns what pipeline is being used
    public double pipelineValue() {
      return table.getEntry("getpipe").getDouble(0.0);
    }

    //Returns a distance in feet ***used to change pipelines
    public double getDistance() {
      double distance = 0.0;

      switch ((int)pipelineValue()) {

        //Uses x1 Pipeline
        case 0:
        distance = ((17.82981 * (Math.pow(getArea(), -0.5229046))));
        break;

        //Uses x2 pipeline
        case 1:
        distance = (12.70763 + (31.89463 * Math.exp(-0.4912732 * getArea())));
        break;

        //Uses long-range x2 pipeline
        case 2:
        distance = (20.05851 + (43.86339 * Math.exp(-1.092591 * getArea())));
        break;
        
      }

      return distance;

    }

    //Changes the Pipeline's zoom (x1 or x2) depending on distance
    public void setPipeline() {

      //Changes pipeline back to x1 zoom when the limelight moves within 15 feet
      if (pipelineEntry.getDouble(0.0) != 0.0 && getDistance() < 14.5) {
        pipelineEntry.setNumber(0.0);
      }

      //Changes pipeline to x2 zoom when the limelight moves 15 feet away
      if (pipelineEntry.getDouble(0.0) != 1.0 && getDistance() > 15 && getDistance() < 24.5) {
        pipelineEntry.setNumber(1.0);
      }
       
      //Changes pipeline at 25 feet to improve long-range distance
      if (pipelineEntry.getDouble(0.0) != 2.0 && getDistance() > 25) {
         pipelineEntry.setNumber(2.0);
      }

    }

  }
