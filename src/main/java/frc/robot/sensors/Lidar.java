package frc.robot.sensors;

import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Subsystem;



public class Lidar extends SerialPort implements Subsystem {

    //private static int dataBit = 8;
    //private static int stopBit = 1;
    private byte[] packet = new byte[9];

    public Lidar() 
    {
        super(115200, Port.kMXP);
        //Baud Rate, Port #, Data Bit, Parity, Stop Bit
        setReadBufferSize(9);
        // 9 bits are stored before it gets read
    }

    public void periodic()
    {
        SmartDashboard.putNumber("lidar get in feet",((get()/2.54)/12));
        //inserts boolean labeling lidar distance on Smart Dashboard
    }

    private short combine(byte one, byte two) 
    {
        return (short) ((one & 0xFF) | (two << 8));
    }


    //should return the distance in centimeters
    public double get()
    {
        reset();
        packet = read(9);
        if(packet.length > 5)
        {
            if (packet[0] == 0x59 && packet[1] == 0x59) 
            {
                //SmartDashboard.putBoolean("data pos incorrect",false);
                return combine(packet[2], packet[3]);
                //Returns false if the data transfer is correct
            } 
            else{
                //SmartDashboard.putBoolean("data pos incorrect", true);
                //returns true if the lidar is not returning data right
            }
        }
        reset();
        return 0;
        //resets the packet and starts over again
    }
}
