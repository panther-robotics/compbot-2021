package frc.robot.sensors.pixy;

import edu.wpi.first.wpilibj.smartdashboard.*;
import frc.robot.sensors.pixy.Vision;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
import edu.wpi.first.wpilibj.I2C;
import frc.robot.sensors.pixy.Pixy2.Objective;

public class Vision implements Subsystem {
    private Pixy2 pixy = new Pixy2(I2C.Port.kOnboard, 0x54, Objective.BLOB_RECOGNITION, "pixy2");
    public static Blob[] blobs;

    public Vision(){
        Robot.scheduler.registerSubsystem(this);
    }

    public void periodic(){
        pixy.update();
        blobs = pixy.getBlobs();
        SmartDashboard.putNumber("pixy blob count", blobs.length);
        if(blobs.length>0){
             /*SmartDashboard.putNumber("Blob X:", blobs[0].getX());
             SmartDashboard.putNumber("Blob Y:", blobs[0].getY());
             SmartDashboard.putNumber("Blob W:", blobs[0].getWidth());
             SmartDashboard.putNumber("Blob H", blobs[0].getHeight());*/
        }
    }
}