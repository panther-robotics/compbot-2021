
package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
// import com.ctre.phoenix.motorcontrol.MotorCommutation;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.collector.RotateStorage;
// import frc.robot.sensors.Lidar;

public class Shooter implements Subsystem {
    public static final double hoodTickLimit = 153300.0;
    public RotateStorage rotateStorageNeg;
   
    public DigitalInput hoodLimit = new DigitalInput(RobotMap.HOOD_LIMIT);
    public DigitalInput hallEffect = new DigitalInput(RobotMap.HALL_EFFECT_SENSOR_PORT);
    CANSparkMax shootingMotorOne = new CANSparkMax(RobotMap.SHOOTING_MOTOR_ONE, MotorType.kBrushless);
    CANSparkMax shootingMotorTwo = new CANSparkMax(RobotMap.SHOOTING_MOTOR_TWO, MotorType.kBrushless);
    //WPI_TalonSRX hoodMotor = new WPI_TalonSRX(RobotMap.HOOD_MOTOR_PORT);
    //WPI_TalonSRX turretMotor = new WPI_TalonSRX(RobotMap.TURRET_MOTOR_PORT);
    public int smoothing = 2;
    CANEncoder pidEncoder;
    CANPIDController pidController;
    double setPoint = 0;
    
    public void periodic() {
        //SmartDashboard.putNumber("Hood encoder", hoodMotor.getSelectedSensorPosition());
        //SmartDashboard.putNumber("Hood velocity", hoodMotor.getSelectedSensorVelocity());
        SmartDashboard.putNumber("Turret encoder", Robot.shooter.getEncoderValue());
        //SmartDashboard.putNumber("hood input direction", Robot.oi.getShooterRightJoystickY());
        SmartDashboard.putNumber("Shooter velocity", getRampSpeed());
        SmartDashboard.putBoolean("Hood limit switch", hoodLimit.get());
    }
    public Shooter() {
        rotateStorageNeg = new RotateStorage(false);

        shootingMotorTwo.set(0);
        shootingMotorOne.set(0);
        shootingMotorOne.setIdleMode(IdleMode.kCoast);
        shootingMotorTwo.setIdleMode(IdleMode.kCoast);
        shootingMotorOne.setInverted(true);
        shootingMotorTwo.follow(shootingMotorOne, true);
        pidEncoder = shootingMotorOne.getEncoder();
        pidController = shootingMotorOne.getPIDController();
        pidController.setFeedbackDevice(pidEncoder);
        stopRamp();
        updateConstants();
        /*
        hoodMotor.setInverted(false);
        hoodMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, Constants.hoodPIDLoopIDx, Constants.hoodTimeoutMs);
        hoodMotor.configNeutralDeadband(0.001, Constants.hoodTimeoutMs);
        hoodMotor.setSensorPhase(false);
        hoodMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, 10, Constants.hoodTimeoutMs);
        hoodMotor.configNominalOutputForward(0, Constants.hoodTimeoutMs);
		hoodMotor.configNominalOutputReverse(-1, Constants.hoodTimeoutMs);
		hoodMotor.configPeakOutputForward(1, Constants.hoodTimeoutMs);
        hoodMotor.configPeakOutputReverse(-1, Constants.hoodTimeoutMs);
        hoodMotor.selectProfileSlot(Constants.hoodSlotIdx, Constants.hoodPIDLoopIDx);
        hoodMotor.config_kP(Constants.hoodSlotIdx, Constants.hoodkP, Constants.hoodTimeoutMs);
        hoodMotor.config_kI(Constants.hoodSlotIdx, Constants.hoodkI, Constants.hoodTimeoutMs);
        hoodMotor.config_kD(Constants.hoodSlotIdx, Constants.hoodkD, Constants.hoodTimeoutMs);
        hoodMotor.config_kF(Constants.hoodSlotIdx, Constants.hoodkF, Constants.hoodTimeoutMs);
        hoodMotor.configMotionCruiseVelocity(15000, Constants.hoodTimeoutMs);
        hoodMotor.configMotionAcceleration(6000, Constants.hoodTimeoutMs);
        hoodMotor.setSelectedSensorPosition(0);
        hoodMotor.setSelectedSensorPosition(0, Constants.hoodPIDLoopIDx, Constants.hoodTimeoutMs);
        hoodMotor.configMotionSCurveStrength(smoothing);
        */
    }
    
    public void setRampSpeed(double setpoint) {
        pidController.setReference(setpoint, ControlType.kVelocity);  
    }
    public double getRampSpeed(){
        return pidEncoder.getVelocity();
    }
    public void stopRamp() {
        shootingMotorOne.setIdleMode(IdleMode.kCoast);
        shootingMotorTwo.setIdleMode(IdleMode.kCoast);
        shootingMotorOne.set(0);
        shootingMotorTwo.set(0);
    }
    public void updateConstants() {
        pidController.setOutputRange(-1, 1);
        pidController.setP(Constants.kP);
        pidController.setI(Constants.kI);
        pidController.setD(Constants.kD);
        pidController.setFF(Constants.kF);
    }

    //convert lidar to hood things and move the hood
    public void setHood(){
        double targetPos = 0;
        double feet = ((Robot.lidar.get()/2.54)/12);
        //targetPos =
        // do something with the value to get an encoder tick
        //Make sure it does not over extend
        if(targetPos >= hoodTickLimit){
            targetPos = hoodTickLimit;
        }else if (targetPos <= 0){
            targetPos = 96;
        }
        //hoodMotor.set(ControlMode.MotionMagic, targetPos);
    }
    public void setHoodManual(double speed){
       // hoodMotor.setInverted(false);
       /* if(turretMotor.getSelectedSensorPosition()>2000 && turretMotor.getSelectedSensorPosition() < hoodTickLimit){
            hoodMotor.set(ControlMode.PercentOutput, speed*.5);
        }else if(turretMotor.getSelectedSensorPosition() < 0){
            hoodMotor.set(ControlMode.PercentOutput, .5);
        }else if(turretMotor.getSelectedSensorPosition() > hoodTickLimit){
            hoodMotor.set(ControlMode.PercentOutput, -.5);
        }*/
        //hoodMotor.set(ControlMode.PercentOutput, speed*.2);

    }
/*
    public boolean setHoodZeroPos(){
        hoodMotor.setInverted(false);
        hoodMotor.set(ControlMode.PercentOutput, .2);
        if(hoodLimit.get()){
            hoodMotor.set(ControlMode.PercentOutput, 0);
            hoodMotor.setSelectedSensorPosition(0);
            return true;
        }
        return false;
    }
    */
    //Returns the ring encoder's position, in rotations
    public double getEncoderValue(){
        return 0;// ((double)turretMotor.getSelectedSensorPosition()/4096.0);
    }
    public void setOrigin(){
        //turretMotor.setSelectedSensorPosition(0);
    }
    /*
    //Sets the motor to a defined speed
    public void moveTurret(double speed){
        if (getEncoderValue() < 2.1 
            && getEncoderValue() > - 2.3){
                    turretMotor.set(speed* -0.5);
            }
            else if (getEncoderValue() > 2.1){
                turretMotor.set(-0.5);
            }
            else if (getEncoderValue() < 2.3){
               turretMotor.set(0.5);
            }
    }
    */
    /*
    //Sets the motor to a defined speed
    public void moveTurretWithLimelight(double speed){
        if (getEncoderValue() < 2.1 
            && getEncoderValue() > - 2.3){
                    turretMotor.set(speed);
            }
            else if (getEncoderValue() > 2.1){
                turretMotor.set(-0.5);
            }
            else if (getEncoderValue() < 2.3){
                turretMotor.set(0.5);
            }
    }
    */
    public void shootAll(){
        setRampSpeed(4000);
        //Robot.collector.startFrontCollector();
        SmartDashboard.putNumber("ramp speed", getRampSpeed());
        if(getRampSpeed() > 3600 && getRampSpeed() < 4400) {
                    Robot.collector.shootFromMag();
            }
    }
    public void shootOne(){
        setRampSpeed(4000);
        if(getRampSpeed() < 4050 && getRampSpeed() > 4000){
            Robot.collector.shootFromMag();
            rotateStorageNeg.schedule();
        }else{
            Robot.collector.stopMovingCollector();
        }
    }
}