package frc.robot.subsystems;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
//import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj2.command.Subsystem;
// import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.collector.RotateStorage;

public class Collector implements Subsystem{   
    
    boolean rotating = false;
    private int smoothing = 6;
    private static final double ticksPerDegrees = (4096/360.0);
    public DigitalInput ballCollected = new DigitalInput(RobotMap.BALL_COLLECTED_IR);
    DigitalInput ballChambered = new DigitalInput(RobotMap.BALL_CHAMBERED_IR);
    DigitalInput posOne = new DigitalInput(RobotMap.POSITION_ONE_IR);
    DigitalInput posTwo = new DigitalInput(RobotMap.POSITION_TWO_IR);
    DigitalInput posThree = new DigitalInput(RobotMap.POSITION_THREE_IR);
    DigitalInput posFour = new DigitalInput(RobotMap.POSITION_FOUR_IR);

    public DigitalInput hallEffectStorage = new DigitalInput(RobotMap.STORAGE_HALL_EFFECT);

    WPI_TalonSRX collectorMotorBottom = new WPI_TalonSRX(RobotMap.COLLECTOR_BOTTOM_MOTOR_PORT);
    public WPI_VictorSPX collectorMotorTop = new WPI_VictorSPX(RobotMap.COLLECTOR_TOP_MOTOR_PORT);
    WPI_VictorSPX collectorMotorBack = new WPI_VictorSPX(RobotMap.COLLECTOR_MID_MOTOR_PORT);
    public WPI_TalonSRX storageMotor = new WPI_TalonSRX(RobotMap.BALL_STORAGE_MOTOR_PORT);
    DoubleSolenoid collectorSolenoid = new DoubleSolenoid(
        RobotMap.COLLECTOR_SOLENOID_PORT_FORWARDS, RobotMap.COLLECTOR_SOLENOID_PORT_BACK);
    public boolean direction = false;

    public static RotateStorage rotateStoragePos;

    @Override
    public void periodic() {
        SmartDashboard.putNumber("Storage Positions Degree", storageMotor.getSelectedSensorPosition()*(-360.0/4096.0));
        SmartDashboard.putNumber("ticks", storageMotor.getSelectedSensorPosition());
    }

    public Collector(){
        collectorMotorBottom.set(0);
        collectorMotorTop.set(0);
        collectorMotorBack.set(0);
        collectorMotorBottom.setInverted(false);
        collectorMotorBottom.setInverted(false);
        collectorMotorBottom.setInverted(false);
        collectorMotorBack.setNeutralMode(NeutralMode.Brake);
        collectorMotorBottom.setNeutralMode(NeutralMode.Brake);
        collectorMotorTop.setNeutralMode(NeutralMode.Brake);

        storageMotor.setInverted(false);
        storageMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, Constants.tPIDLoopIDx, Constants.tTimeoutMs);
        storageMotor.configNeutralDeadband(0.1, Constants.tTimeoutMs);
        storageMotor.setSensorPhase(false);
        storageMotor.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, 10, Constants.tTimeoutMs);
        storageMotor.setNeutralMode(NeutralMode.Brake);
        storageMotor.configNominalOutputForward(0, Constants.tTimeoutMs);
		storageMotor.configNominalOutputReverse(0, Constants.tTimeoutMs);
		storageMotor.configPeakOutputForward(.8, Constants.tTimeoutMs);
        storageMotor.configPeakOutputReverse(-.8, Constants.tTimeoutMs);
        storageMotor.selectProfileSlot(Constants.tSlotIdx, Constants.tPIDLoopIDx);
        storageMotor.config_kF(Constants.tSlotIdx, Constants.tkF, Constants.tTimeoutMs);
        storageMotor.config_kP(Constants.tSlotIdx, Constants.tkP, Constants.tTimeoutMs);
        storageMotor.config_kI(Constants.tSlotIdx, Constants.tkI, Constants.tTimeoutMs);
        storageMotor.config_kD(Constants.tSlotIdx, Constants.tkD, Constants.tTimeoutMs);
        storageMotor.configMotionCruiseVelocity(200, Constants.tTimeoutMs);
        storageMotor.configMotionAcceleration(1000, Constants.tTimeoutMs);
        storageMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute);
        storageMotor.configMotionSCurveStrength(smoothing);
        storageMotor.setNeutralMode(NeutralMode.Brake);
        rotateStoragePos = new RotateStorage(true);
    }

    public int storageMotorPosition(){
        return storageMotor.getSelectedSensorPosition();
    }
    private int desiredDegrees;

    public void extendSolenoid(){collectorSolenoid.set(Value.kForward);}
    public void retractSolenoid(){collectorSolenoid.set(Value.kReverse);}

    //checks ir sensors on the ball storage and returns if there is a ball at the position at the given parameter
    public boolean updateBallStorageArray(int pos){
        boolean[] ballStorageArray = {posOne.get(), posTwo.get(), posThree.get(), posFour.get(), ballChambered.get()};
        return ballStorageArray[pos - 1];
    }
    

    public boolean checkRotating(){
        if(storageMotor.getSelectedSensorVelocity()>50){
            return true;
        }
        return false;
    }

    public void startCollecting(){
        //towards storage
        collectorMotorBottom.set(.6);
        //torwards storage
        collectorMotorTop.set(-.6);
        //towards storage -
        collectorMotorBack.set(0);
        storageMotor.set(0.6);
    }

    public void startFrontCollector(){
        collectorMotorTop.set(-0.6);
    }
    public void stopMovingCollector(){
        collectorMotorBottom.set(0);
        collectorMotorTop.set(0);
        collectorMotorBack.set(0);
        storageMotor.set(0);
    }
    public void evacChamber(){
        collectorMotorBottom.set(-.5);
        collectorMotorTop.set(1);
    }
    public void evacAll(){
        collectorMotorBottom.set(-.5);
        collectorMotorTop.set(1);
        collectorMotorBack.set(1);
    }
    public void toPosOne() {
        collectorMotorBottom.set(.8);
        collectorMotorBack.set(-.2);
    }
    public void shootFromMag(){
        collectorMotorBottom.set(-.5);
        //collectorMotorTop.set(-1);
        collectorMotorBack.set(1);
        storageMotor.set(-1);
    }
    public void shootFromChamber(){
        collectorMotorBack.set(1);
        collectorMotorTop.set(-1);
    }
    public void getOut(){
        collectorMotorTop.set(1);
    }
}
