package frc.robot.subsystems;

// import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.CANSparkMax;
// import com.revrobotics.SparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
// import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Subsystem;
// import frc.robot.Robot;
import frc.robot.RobotMap;

public class Lift implements Subsystem 
{
    public CANSparkMax liftMotor = new CANSparkMax(RobotMap.WINCH_MOTOR_PORT, MotorType.kBrushless);
    boolean extended = false;
    DoubleSolenoid LiftSolenoid = new DoubleSolenoid(RobotMap.LIFT_SOLENOID_PORT_FORWARDS, 
        RobotMap.LIFT_SOLENOID_PORT_BACK);

    public Lift(){
        liftMotor.set(0);
    }

    @Override
    public void periodic(){
       // SmartDashboard.putBoolean("Lift Extended", extended);
    }
    //Drives the winch motor a set speed
    public void liftWithMotor(double speed){
          liftMotor.set(speed);
    }
    //Lowers the pistons
    public void freeFloatPistons(){
        LiftSolenoid.set(Value.kReverse);
        extended = false;
    }
    //Raises the pistons
    public void raisePistons(){
        LiftSolenoid.set(Value.kForward);
    }

}
