
/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

// import com.ctre.phoenix.motorcontrol.ControlMode;
// import com.ctre.phoenix.motorcontrol.DemandType;
// import com.ctre.phoenix.motorcontrol.FeedbackDevice;
// import com.ctre.phoenix.motorcontrol.FollowerType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
// import com.ctre.phoenix.motorcontrol.RemoteSensorSource;
// import com.ctre.phoenix.motorcontrol.StatusFrame;
// import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.TalonFXInvertType;
import com.ctre.phoenix.motorcontrol.can.TalonFXConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
// import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.sensors.PigeonIMU;
// import com.ctre.phoenix.sensors.PigeonIMU_StatusFrame;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
// import edu.wpi.first.wpilibj.Talon;
// import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.drivetrain.Drive;

/**
 * Add your docs here.
 */
public class Drivetrain implements Subsystem {  
    PigeonIMU pig = new PigeonIMU(RobotMap.COLLECTOR_BOTTOM_MOTOR_PORT);
    WPI_TalonFX leftTopMotor = new WPI_TalonFX(RobotMap.LEFT_TOP_MOTOR_PORT);
    WPI_TalonFX leftMidMotor = new WPI_TalonFX(RobotMap.LEFT_MID_MOTOR_PORT);
    WPI_TalonFX leftBottomMotor = new WPI_TalonFX(RobotMap.LEFT_BOTTOM_MOTOR_PORT);
    WPI_TalonFX rightTopMotor = new WPI_TalonFX(RobotMap.RIGHT_TOP_MOTOR_PORT);
    WPI_TalonFX rightMidMotor = new WPI_TalonFX(RobotMap.RIGHT_MID_MOTOR_PORT);
    WPI_TalonFX rightBottomMotor = new WPI_TalonFX(RobotMap.RIGHT_BOTTOM_MOTOR_PORT);
    private double metersPerTick=0.0001;
    private double x=0; //in meters
    private double y=0; //in meters
    SpeedControllerGroup leftMotors = new SpeedControllerGroup(leftTopMotor, leftMidMotor, leftBottomMotor);
    SpeedControllerGroup rightMotors = new SpeedControllerGroup(rightTopMotor,rightMidMotor, rightBottomMotor);
    DifferentialDrive drive = new DifferentialDrive(leftMotors, rightMotors);

    TalonFXConfiguration leftConfig = new TalonFXConfiguration();
    TalonFXConfiguration rightConfig = new TalonFXConfiguration();
    TalonFXInvertType leftInvert = TalonFXInvertType.CounterClockwise;
    TalonFXInvertType rightInvert = TalonFXInvertType.Clockwise;

    public void periodic(){
        SmartDashboard.putNumber("rightTop encoder tick", rightTopMotor.getSelectedSensorPosition());
    }

    public void innit(){
        rightTopMotor.setSelectedSensorPosition(0);
        rightMidMotor.setSelectedSensorPosition(0);
        rightBottomMotor.setSelectedSensorPosition(0);
        leftTopMotor.setSelectedSensorPosition(0);
        leftMidMotor.setSelectedSensorPosition(0);
        leftBottomMotor.setSelectedSensorPosition(0);
    }

    public Drivetrain(){
        // Robot.scheduler.registerSubsystem(this);
        // Robot.scheduler.setDefaultCommand(this, new Drive(this));
        leftTopMotor.configFactoryDefault();
        leftMidMotor.configFactoryDefault();
        leftBottomMotor.configFactoryDefault();
        rightTopMotor.configFactoryDefault();
        rightMidMotor.configFactoryDefault();
        rightBottomMotor.configFactoryDefault();
        leftTopMotor.setNeutralMode(NeutralMode.Brake);
        leftMidMotor.setNeutralMode(NeutralMode.Brake);
        leftBottomMotor.setNeutralMode(NeutralMode.Brake);
        rightTopMotor.setNeutralMode(NeutralMode.Brake);
        rightMidMotor.setNeutralMode(NeutralMode.Brake);
        rightBottomMotor.setNeutralMode(NeutralMode.Brake);
        leftMotors.setInverted(false);
        rightMotors.setInverted(false);
        rightTopMotor.setSelectedSensorPosition(0);
        pig.configFactoryDefault();
    }
     
        /*
        leftConfig.primaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.IntegratedSensor.toFeedbackDevice();
        rightConfig.remoteFilter0.remoteSensorDeviceID = leftTopMotor.getDeviceID();
        rightConfig.remoteFilter0.remoteSensorSource = RemoteSensorSource.TalonFX_SelectedSensor;
        setRobotDistanceConfigs(rightInvert, rightConfig);
        Distance
        rightConfig.slot0.kF = Constants.kDistanceF;
        rightConfig.slot0.kP = Constants.kDistanceP;
        rightConfig.slot0.kI = Constants.kDistanceI;
        rightConfig.slot0.kD = Constants.kDistanceD;
        rightConfig.slot0.integralZone = Constants.kDistanceIZone;
        rightConfig.slot0.closedLoopPeakOutput = Constants.kDistancePeakOutput;

        rightConfig.remoteFilter1.remoteSensorDeviceID = pig.getDeviceID();
        rightConfig.remoteFilter1.remoteSensorSource = RemoteSensorSource.Pigeon_Yaw;
        rightConfig.auxiliaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.RemoteSensor1.toFeedbackDevice();
        rightConfig.auxiliaryPID.selectedFeedbackCoefficient = 3600.0;

        rightConfig.slot1.kF = Constants.kTurnF;
        rightConfig.slot1.kP = Constants.kTurnP;
        rightConfig.slot1.kI = Constants.kTurnI;
        rightConfig.slot1.kD = Constants.kTurnD;
        rightConfig.slot1.integralZone = Constants.kTurnIZone;
        rightConfig.slot1.closedLoopPeakOutput = Constants.kTurnPeakOutput;
        leftConfig.neutralDeadband = Constants.kNeutralDeadband;
        rightConfig.neutralDeadband = Constants.kNeutralDeadband;

        int closedLoopTimeMs = 1;
        rightConfig.slot0.closedLoopPeriod = closedLoopTimeMs;
        rightConfig.slot1.closedLoopPeriod = closedLoopTimeMs;
        rightConfig.slot2.closedLoopPeriod = closedLoopTimeMs;
        rightConfig.slot3.closedLoopPeriod = closedLoopTimeMs;

        rightConfig.motionAcceleration = 2000;
        rightConfig.motionCruiseVelocity = 2000;

        leftTopMotor.configAllSettings(leftConfig);
        rightTopMotor.configAllSettings(rightConfig);

        rightTopMotor.setStatusFramePeriod(StatusFrame.Status_12_Feedback1, 20, Constants.kDistanceTimeoutMs);
        rightTopMotor.setStatusFramePeriod(StatusFrame.Status_13_Base_PIDF0, 20, Constants.kDistanceTimeoutMs);
        rightTopMotor.setStatusFramePeriod(StatusFrame.Status_14_Turn_PIDF1, 20, Constants.kDistanceTimeoutMs);
        rightTopMotor.setStatusFramePeriod(StatusFrame.Status_10_Targets, 10, Constants.kDistanceTimeoutMs);
        pig.setStatusFramePeriod(PigeonIMU_StatusFrame.CondStatus_9_SixDeg_YPR, 5, Constants.kDistanceTimeoutMs);
        rightTopMotor.configMotionSCurveStrength(6);
        
    }

    public void driveStraight(double targetRotations, double turnAngle){
        rightTopMotor.selectProfileSlot(Constants.kDistanceSlotIdx, Constants.PID_PRIMARY);
        rightTopMotor.selectProfileSlot(Constants.kTurnSlotIdx, Constants.PID_TURN);
        //Convert from rotations to sensorunits and makes call to Motion Magic
        //take into account the gear ratio
        //below would take an input of rotations
        //targetRotations = targetRotations*2048;
        rightTopMotor.set(ControlMode.MotionMagic, targetRotations, DemandType.AuxPID, turnAngle);
        leftTopMotor.follow(rightTopMotor, FollowerType.AuxOutput1);
    }
    public void zeroSensors(){
        //All sensors are set to 0
        leftTopMotor.getSensorCollection().setIntegratedSensorPosition(0, Constants.kDistanceTimeoutMs);
        rightTopMotor.getSensorCollection().setIntegratedSensorPosition(0, Constants.kDistanceTimeoutMs);
        pig.setYaw(0, Constants.kDistanceTimeoutMs);
        pig.setAccumZAngle(0, Constants.kTurnTimeoutMs);
    }
    public void zeroDistance(){
        leftTopMotor.getSensorCollection().setIntegratedSensorPosition(0, Constants.kDistanceTimeoutMs);
        rightTopMotor.getSensorCollection().setIntegratedSensorPosition(0, Constants.kDistanceTimeoutMs);
    } 
    public void setRobotDistanceConfigs(TalonFXInvertType masterInvertType, TalonFXConfiguration masterConfig){
        if(masterInvertType == TalonFXInvertType.Clockwise){
            masterConfig.diff0Term = TalonFXFeedbackDevice.IntegratedSensor.toFeedbackDevice();
            masterConfig.diff1Term = TalonFXFeedbackDevice.RemoteSensor0.toFeedbackDevice();
            masterConfig.primaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.SensorDifference.toFeedbackDevice();
        }else{
            masterConfig.sum0Term = TalonFXFeedbackDevice.RemoteSensor0.toFeedbackDevice();
            masterConfig.sum1Term = TalonFXFeedbackDevice.IntegratedSensor.toFeedbackDevice();
            masterConfig.primaryPID.selectedFeedbackSensor = TalonFXFeedbackDevice.SensorSum.toFeedbackDevice();
        }
        masterConfig.primaryPID.selectedFeedbackCoefficient = .5;
    }
  */
    //regular default drive method at 60% speed
    public void drive(double y, double z) {
        y = y*(.9);
        z = z*(.9);
        drive.arcadeDrive(-y,z);
    }
    //Method to move an input number of inches for auto
    public boolean moveInches(double x){
        rightTopMotor.setSelectedSensorPosition(0);
        double target = x*Constants.drivetrainTicksPerInch;
        drive.arcadeDrive(0, .7*Math.signum(x));
        //x *= 8.61;
        if(Math.abs(rightTopMotor.getSelectedSensorPosition()) >= Math.abs(target)){
            drive.arcadeDrive(0, 0);
            return true;
        }
        return false;
    }
    //Rotate a number of input inches
    public boolean rotateInches(double x){
        rightTopMotor.setSelectedSensorPosition(0);
        double target = x*Constants.drivetrainTicksPerInch;
        drive.arcadeDrive(.7*Math.signum(x), 0);
        //x *= 8.61;
        if(Math.abs(rightTopMotor.getSelectedSensorPosition()) >= Math.abs(target)){
            drive.arcadeDrive(0, 0);
            return true;
        }
        return false;
    }
    //updates X and Y position on the field when using TalonFX's as the drive motor controllers
    public void updatePosition(){
        double averageTicks=(leftTopMotor.getSelectedSensorVelocity()+leftMidMotor.getSelectedSensorVelocity()+leftBottomMotor.getSelectedSensorVelocity()+
        rightTopMotor.getSelectedSensorVelocity()+rightMidMotor.getSelectedSensorVelocity()+rightBottomMotor.getSelectedSensorVelocity())/6;
        double distanceMoved=averageTicks*metersPerTick;
        double xHeading = getXHeading();
        x+= Math.cos(xHeading)*distanceMoved;
        y+= Math.sin(xHeading)*distanceMoved;
    }
    //returns x heading
    public double getXHeading(){
        double[] xyz = new double[3];
        //pigeon.getRawGyro(xyz);
        return xyz[0];
    }

}
