package frc.robot;

public class RobotMap {
    //Driver Controllers
    public static final int DRIVER_XBOX_PORT = 3;
    public static final int SHOOTER_XBOX_PORT = 2;
    
    //CAN Ports
    public static final int LEFT_TOP_MOTOR_PORT = 11;
    public static final int LEFT_MID_MOTOR_PORT = 12;
    public static final int LEFT_BOTTOM_MOTOR_PORT = 13;
    public static final int RIGHT_TOP_MOTOR_PORT = 14;
    public static final int RIGHT_MID_MOTOR_PORT = 15;
    public static final int RIGHT_BOTTOM_MOTOR_PORT = 16;

    public static final int COLLECTOR_TOP_MOTOR_PORT = 8;
    public static final int COLLECTOR_BOTTOM_MOTOR_PORT = 19;    
    public static final int COLLECTOR_MID_MOTOR_PORT = 7;
    
    public static final int BALL_STORAGE_MOTOR_PORT = 17;

    public static final int SHOOTING_MOTOR_ONE = 6;
    public static final int SHOOTING_MOTOR_TWO = 4;
   // public static final int TURRET_MOTOR_PORT = 10;
    //public static final int HOOD_MOTOR_PORT = 5;

    public static final int WINCH_MOTOR_PORT = 18;

    //DIO and PWM Ports
	public static final int HALL_EFFECT_SENSOR_PORT = 6;
    public static final int BALL_COLLECTED_IR = 0;
    public static final int BALL_CHAMBERED_IR = 1;
    public static final int POSITION_ONE_IR = 2;
    public static final int POSITION_TWO_IR = 3;
    public static final int POSITION_THREE_IR = 4;
    public static final int POSITION_FOUR_IR = 5;

    public static final int STORAGE_HALL_EFFECT = 9;
    public static final int HOOD_LIMIT = 8;

    //PCM Ports
	public static final int COLLECTOR_SOLENOID_PORT_FORWARDS = 3;
	public static final int COLLECTOR_SOLENOID_PORT_BACK = 7;
    public static final int LIFT_SOLENOID_PORT_FORWARDS = 1;
    public static final int LIFT_SOLENOID_PORT_BACK = 2;

}
